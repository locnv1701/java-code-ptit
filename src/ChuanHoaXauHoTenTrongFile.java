import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ChuanHoaXauHoTenTrongFile {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("DATA.in");
        Scanner sc = new Scanner(file);
        ArrayList<String> result = new ArrayList<>();
        while (sc.hasNextLine()) {
            String str = sc.nextLine();
            str = str.toLowerCase();
            str = str.trim();
            str = str.replaceAll("\\s+", " ");
            String[] token = str.split(" ");
            String res = "";
            if(str.equals("end")){
                break;
            }
            for (int i = 0; i < token.length; i++) {
                res += String.valueOf(token[i].charAt(0)).toUpperCase() + token[i].substring(1,token[i].length());
                res += " ";
            }
            result.add(res);
        }
        for (String s : result) {
            System.out.println(s);
        }
    }
}

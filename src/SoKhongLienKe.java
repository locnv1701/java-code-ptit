import java.util.Scanner;

public class SoKhongLienKe {
    public static String check(String s) {
        int sum = 0;
        for (int i = 0; i < s.length() - 1; i++) {
            sum += Integer.parseInt(s.substring(i, i + 1));
            if (Math.abs((int) s.charAt(i) - (int) s.charAt(i + 1)) != 2)
                return "NO";
        }
        sum += Integer.parseInt(s.substring(s.length() - 1));
        if (sum % 10 == 0)
            return "YES";
        else return "NO";
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String s = sc.nextLine();
            System.out.println(check(s));
        }
    }
}

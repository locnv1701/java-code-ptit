import java.util.*;
public class DienThoaiCucGach {
    public static boolean tn(String s){
        StringBuilder str = new StringBuilder(s);
        return str.reverse().toString().equals(s);
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0){
            String s = sc.next().toUpperCase();
            StringBuilder res = new StringBuilder();
            for(int i = 0; i < s.length(); i++){
                switch (s.charAt(i)) {
                    case 'A', 'B', 'C' -> res.append("2");
                    case 'D', 'E', 'F' -> res.append("3");
                    case 'G', 'H', 'I' -> res.append("4");
                    case 'J', 'K', 'L' -> res.append("5");
                    case 'M', 'N', 'O' -> res.append("6");
                    case 'P', 'Q', 'R', 'S' -> res.append("7");
                    case 'T', 'U', 'V' -> res.append("8");
                    default -> res.append("9");
                }
            }
            String tmp = res.toString();
            if(tn(tmp)){
                System.out.println("YES");
            }
            else{
                System.out.println("NO");
            }
        }
    }
}
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class ThuanNghichTrong2file {
    public static boolean ThuanNghich(String s) {
        StringBuilder stringBuilder = new StringBuilder(s);
        return stringBuilder.reverse().toString().equals(s);
    }

    public static boolean soLe(int n) {
        while(n != 0) {
            int p = n%10;
            if(p%2==0)
                return false;
            n /= 10;
        }
        return true;
    }
    public static boolean soChuSo(int n) {
        int count = 0;
        while(n != 0) {
            n /= 10;
            count++;
        }
        if(count <= 1) return false;
        else if(count%2 == 0) return false;
        else return true;
    }

    public static void main(String[] args) throws Exception {
        FileInputStream fileInp = new FileInputStream("NHIPHAN.in");
        ObjectInputStream objIS = new ObjectInputStream(fileInp);
        ArrayList<Integer> a = (ArrayList<Integer>) objIS.readObject();
        objIS.close();

        fileInp = new FileInputStream("DATA2.in");
        objIS = new ObjectInputStream(fileInp);
        ArrayList<Integer> b = (ArrayList<Integer>) objIS.readObject();
        objIS.close();

        int count = 10;
        for (int i = 0; i < 1000000; i++) {
            if (ThuanNghich(String.valueOf(i)) && soLe(i) && soChuSo(i)) {
                int counta = Collections.frequency(a, i);
                int countb = Collections.frequency(b, i);
                if (counta != 0 && countb != 0) {
                    System.out.println(i + " " + counta + countb);
                    count--;
                }
                if (count == 0) break;
            }
        }
    }
}
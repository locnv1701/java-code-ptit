import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SapXepNoiBotLietKeNguoc {
    public static void output(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static void swap(int arr[], int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
            }
            int count = 0;
            ArrayList<String> res = new ArrayList<>();
            for (int i = 0; i < n - 1; i++) {
                int check = 1;
                for (int j = 0; j < n - i - 1; j++) {
                    if (arr[j] > arr[j + 1]) {
                        swap(arr, j, j + 1);
                        check = 0;
                    }
                }
                if (check == 0) {
                    count++;
                    String tmp = "Buoc " + count + ":";
                    for (int k = 0; k < n; k++) {
                        tmp = tmp + " " + arr[k];
                    }
                    res.add(tmp);
                }



            }Collections.reverse(res);
            res.forEach(System.out::println);
        }

    }
}


import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.Scanner;

public class TachDoiVaTinhTong {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("DATA.in"));
        String s = sc.nextLine();
        while (s.length() > 1) {
            BigInteger b1 = new BigInteger(s.substring(0, s.length() / 2));
            BigInteger b2 = new BigInteger(s.substring(s.length() / 2));
            BigInteger b3 = b1.add(b2);
            System.out.println(b3);
            s = b3.toString();
        }
    }
}

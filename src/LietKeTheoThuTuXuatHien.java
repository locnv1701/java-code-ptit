import java.io.*;
import java.util.*;

public class LietKeTheoThuTuXuatHien {
    public static void main(String[] args) throws Exception {
        FileInputStream fileInp = new FileInputStream("NHIPHAN.in");
        ObjectInputStream objIS = new ObjectInputStream(fileInp);
        ArrayList<String> a = (ArrayList<String>) objIS.readObject();
        objIS.close();
        ArrayList<String> vanBanBin = new ArrayList<>();
        for(String j:a){
            String[] words = j.trim().toLowerCase().split("\\s+");
            for(String i:words){
                vanBanBin.add(i);
            }
        }

//        System.out.println(a.toString());

        Scanner sc = new Scanner(new File("VANBAN.in"));
        Set<String> set = new HashSet<>();
        ArrayList<String> arrayList = new ArrayList<>();
        while (sc.hasNext()) {
            String tmp = sc.next().toLowerCase();
            if (tmp.equals(""))
                continue;
            if (!set.contains(tmp)) {
                set.add(tmp);
                arrayList.add(tmp);
            }

        }
        for (String s1 : arrayList) {
            for (String s2 : vanBanBin) {
                if (s1.equalsIgnoreCase(s2)) {
                    System.out.println(s1);
                    break;
                }
            }
        }
    }
}
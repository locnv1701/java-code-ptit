import java.util.Scanner;

public class BienSoDep {
    public static boolean tangChat(String s) {
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) <= s.charAt(i - 1))
                return false;
        }
        return true;
    }

    public static boolean nam(String s) {
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) != s.charAt(i - 1))
                return false;
        }
        return true;
    }

    public static boolean haiBa(String s) {
        if (s.charAt(0) != s.charAt(1))
            return false;
        else if (s.charAt(1) != s.charAt(2))
            return false;
        else return s.charAt(3) == s.charAt(4);
    }

    public static boolean locPhat(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != '6' && s.charAt(i) != '8')
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String s = sc.nextLine();
            String tmp = s.substring(5,8) + s.substring(9);
            if (locPhat(tmp) || haiBa(tmp) || nam(tmp) || tangChat(tmp))
                System.out.println("YES");
            else System.out.println("NO");
        }
    }
}


import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class SoKhacNhauTrongFile3 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("DATA.in");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ArrayList<Integer> arrayList = (ArrayList<Integer>) objectInputStream.readObject();
        int count[] = new int[1000000];
        Arrays.fill(count, 0);
        for (Integer integer : arrayList) {
            count[integer]++;
        }

        for (int i = 0; i < 1000000; i++) {
            if (count[i] > 0)
                System.out.println(i + " " + count[i]);
        }
    }
}

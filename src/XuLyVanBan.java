import java.util.Scanner;

public class XuLyVanBan {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        StringBuilder input = new StringBuilder();
        while (sc.hasNextLine()) {
            String str = sc.nextLine();
            if (str.equals(""))
                break;
            input.append(str);
        }
        String[] words = input.toString().trim().split("\\?|\\!|\\.");
        for (String word : words) {
            word = word.trim().replaceAll("\\s+", " ").toLowerCase();
            String res = Character.toUpperCase(word.charAt(0)) + word.substring(1);
            System.out.println(res);
        }
    }
}
/*
ky thi LAP TRINH ICPC PTIT  bat dau to chuc     tu     nam 2010. nhu vay, nam nay la          tron 10 nam!
    vay CO PHAI NAM NAY LA KY THI LAN THU 10?        khong phai! nam nay la KY THI LAN THU 11.
 */

import java.util.Scanner;

public class MangDoiXung {
        public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 1; i <= t; i++) {
            int n = sc.nextInt();
            int[] arr = new int[n];
            for (int j = 0; j < n; j++) {
                arr[j] = sc.nextInt();
            }
            boolean check = true;
            for (int j = 0; j < n / 2; j++) {
                if (arr[j] != arr[n - 1 - j])
                    check = false;
            }
            if (check) {
                System.out.println("YES");
            } else
                System.out.println("NO");

        }
    }
}
import java.util.Scanner;

public class TinhSoFibonacci {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 0; i < t; i++) {
            int n = sc.nextInt();
            long [] Fibo = new long[93];
            Fibo[1] = 1;
            Fibo[2] = 1;
            for (int j = 2; j < 93; j++) {
                Fibo[j] = Fibo[j-1] + Fibo[j-2];
            }
            System.out.println(Fibo[n]);

        }
    }
}

import java.util.Scanner;

public class SoUuThe {
    public static String check(String s) {
        int countc = 0;
        int countl = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '0' || s.charAt(i) == '2' || s.charAt(i) == '4' || s.charAt(i) == '6' || s.charAt(i) == '8')
                countc += 1;
            else if (s.charAt(i) == '1' || s.charAt(i) == '3' || s.charAt(i) == '5' || s.charAt(i) == '7' || s.charAt(i) == '9')
                countl += 1;
            else return "INVALID";
        }
        if (s.length() % 2 == 0 && countc > countl)
            return "YES";
        else if (s.length() % 2 != 0 && countc < countl)
            return "YES";
        else return "NO";
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String s = sc.nextLine();
            System.out.println(check(s));
        }
    }
}

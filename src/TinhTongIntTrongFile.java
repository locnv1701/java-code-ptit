import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.Scanner;

public class TinhTongIntTrongFile {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("DATA.in");
        Scanner output = new Scanner(file);
        BigInteger quinn = new BigInteger("0");
        String digit = "[+-]?\\b[0-9]+\\b";
        while (output.hasNext()) {
            String num = output.next();
            if (num.length() <= 11 && num.matches(digit) && Long.parseLong(num) <= Integer.MAX_VALUE && Long.parseLong(num) >= Integer.MIN_VALUE) {
                quinn = quinn.add(new BigInteger(num));
            }
        }
        System.out.println(quinn);
    }
}

import java.util.Scanner;
import java.math.BigInteger;

public class TongSoNguyenLon2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

            BigInteger x = new BigInteger(sc.next().trim());
            BigInteger y = new BigInteger(sc.next().trim());

            BigInteger xy = x.add(y);
            System.out.println(xy);
    }
}

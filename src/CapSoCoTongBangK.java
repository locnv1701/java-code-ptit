import java.util.*;

public class CapSoCoTongBangK {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            long k = sc.nextLong();
            long[] array = new long[n];
            for (int i = 0; i < n; i++) {
                array[i] = sc.nextLong();
            }
            Map<Long, Integer> map = new HashMap<>();
            for (int i = 0; i < n; i++) {
                if (map.containsKey(array[i])) {
                    map.put(array[i], map.get(array[i]) + 1);
                } else {
                    map.put(array[i], 1);
                }
            }

            int result = 0;
            for (int i = 0; i < n; i++) {
                if (map.containsKey(k - array[i])) {
                    map.put(array[i], map.get(array[i]) - 1);
                    result += map.get(k - array[i]);
                    if (map.get(array[i]) == 0) {
                        map.remove(k - array[i]);
                        map.remove(array[i]);
                    }
                }
                else
                    map.remove(array[i]);
            }
            System.out.println(result);
        }
    }
}

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HinhSao {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < t - 1; i++) {
            int x = sc.nextInt();
            int y = sc.nextInt();
            if (map.containsKey(x)) {
                map.put(x, map.get(x) + 1);
            } else map.put(x, 1);
            if (map.containsKey(y)) {
                map.put(y, map.get(y) + 1);
            } else map.put(y, 1);
        }
        boolean quinn = false;
        for (Integer integer : map.keySet()) {
            if (map.get(integer) == t - 1)
                quinn = true;
        }
        if (quinn)
            System.out.println("Yes");
        else System.out.println("No");
    }
}

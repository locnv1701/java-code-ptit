import java.util.Scanner;
import java.util.Stack;

import static java.lang.Math.max;

public class DayNgoacDungDaiNhat {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while(t-->0){
            Stack<Integer> stk = new Stack<>();
            String str = sc.nextLine();
            int max = 0;
            stk.push(-1);
            for (int i = 0; i < str.length(); i++) {
                if(str.charAt(i) == '(')
                    stk.push(i);
                if(str.charAt(i) == ')')
                {
                    stk.pop();
                    if(stk.size()>0){
                        max = max(max, i - stk.peek());
                    }
                    if(stk.size() == 0){
                        stk.push(i);
                    }
                }
            }
            System.out.println(max);
        }
    }
}

import java.util.*;

public class TapTuRiengCuaXau {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        String tmp = sc.nextLine();
        while (t-- > 0) {
            String s1 = sc.nextLine();
            String s2 = sc.nextLine();
            String[] token1 = s1.split(" ");
            Arrays.stream(token1).sorted();
            String[] token2 = s2.split(" ");
            Arrays.stream(token2).sorted();

            Set<String> set = new HashSet<String>();
            Set<String> res = new HashSet<String>();


            for (int i = 0; i < token2.length; i++) {
                set.add(token2[i]);
            }

            for (int j = 0; j < token1.length; j++) {
                if(set.add(token1[j])) {
                    res.add(token1[j]);
                }
            }

            for(Object it : res){
                System.out.print(it + " ");
            }
            System.out.println();
        }
    }
}

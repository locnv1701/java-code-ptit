import java.util.Scanner;

public class SoDep2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0){
            String str = sc.next();
            boolean check = true;
            int sum = 0;
            for (int i = 0; i < str.length(); i++) {
                if(str.charAt(0) != '8' ||  str.charAt(str.length()-1) != '8'){
                    check = false;
//                    System.out.println("loi dau cuoi");
                    break;
                }
                if(str.charAt(i) != str.charAt(str.length()-1 - i)) {
                    check = false;
//                    System.out.println("loi thuan nghich tai " + i);
                    break;
                }
                sum += Character.getNumericValue(str.charAt(i));
            }
            if(sum % 10 != 0){
                check = false;
//                System.out.println("loi chia het cho 10 sum = " + sum);
            }
            if(check == true ){
                System.out.println("YES");
            }
            else
                System.out.println("NO");
        }
    }
}

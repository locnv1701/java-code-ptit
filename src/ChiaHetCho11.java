import java.util.Scanner;

public class ChiaHetCho11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        String tmp = sc.nextLine();
        while(t--> 0){
            String string = sc.nextLine();
            int sum = 0;
            for (int i = 0; i < string.length(); i++) {
                if(i%2==0){
                    sum += (int)string.charAt(i);
                }
                else sum -= (int)string.charAt(i);
            }
            if(sum%11==0){
                System.out.println(1);
            }
            else System.out.println(0);

        }
    }

}

import java.util.Scanner;
import java.util.Arrays;

public class DemSoLanXuatHien {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 1; i <= t; i++) {
            int n = sc.nextInt();
            System.out.println("Test " + i + ": ");
            int[] arr = new int[n];
            int[] count = new int[10000];
            for (int j = 0; j < n; j++) {
                arr[j] = sc.nextInt();
            }
            Arrays.fill(count, 0);
            for (int j = 0; j < n; j++) {
                count[arr[j]]++;
            }
            for (int j = 0; j < n; j++) {
                if (count[arr[j]] > 0) {
                    System.out.println(arr[j] + " xuat hien " + count[arr[j]] + " lan");
                    count[arr[j]] = 0;
                }
            }
        }
    }
}



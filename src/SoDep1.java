import java.util.Scanner;

public class SoDep1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0){
            String str = sc.next();
            boolean check = true;
            for (int i = 0; i < str.length(); i++) {
                if((int)str.charAt(i)%2==1){
                    check = false;
                }
            }
            if(check == true){
                System.out.println("YES");
            }
            else
                System.out.println("NO");
        }
    }
}

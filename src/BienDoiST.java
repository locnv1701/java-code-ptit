import java.util.Scanner;

public class BienDoiST {

    private static long sol(int n, int m){
        if(m <= n)
            return n-m;
        else{
            if(m%2==0)
                return 1+sol(n,m/2);
            else
                return 2+sol(n,(m+1)/2);
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while(test-->0){
            int s = sc.nextInt();
            int t = sc.nextInt();
            System.out.println(sol(s,t));
        }
    }
}

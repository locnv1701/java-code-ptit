import java.util.ArrayList;
import java.util.Scanner;

public class BoSungDaySo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        ArrayList<Integer> arr = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            arr.add(sc.nextInt());
        }
        int b = arr.get(arr.size()-1);
        boolean quinn = false;
        for (int i = 1; i <= b; i++) {
            if (!arr.contains(i)) {
                quinn = true;
                System.out.println(i);
            }
        }
        if (!quinn)
            System.out.println("Excellent!");
    }
}

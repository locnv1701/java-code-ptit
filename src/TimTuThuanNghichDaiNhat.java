import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TimTuThuanNghichDaiNhat {
    public static boolean check(String str) {
        String tmp = "";
        for (int i = str.length() - 1; i >= 0; i--) {
            tmp += str.charAt(i) + "";
        }
        return str.equals(tmp);
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        Map<String, Integer> map = new HashMap<>();
        int max = 0;
        ArrayList<String> stringArrayList = new ArrayList<>();
        while (sc.hasNextLine()) {
            String tmp = sc.nextLine().trim();
            String[] words = tmp.split("\\s+");

            for (String word : words) {
                if (check(word)) {
                    if (word.length() > max) {
                        max = word.length();
                    }
                    if (!map.containsKey(word)) {
                        map.put(word, 1);
                        stringArrayList.add(word);
                    } else
                        map.put(word, map.get(word) + 1);
                }
            }
        }

        int finalMax = max;
        stringArrayList.forEach(s -> {
            if (s.length() == finalMax)
                System.out.println(s + " " + map.get(s));
        });
    }
}
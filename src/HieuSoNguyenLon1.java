import java.util.Scanner;
import java.math.BigInteger;

public class HieuSoNguyenLon1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0){
            BigInteger x = new BigInteger(sc.next().trim());
            BigInteger y = new BigInteger(sc.next().trim());

            BigInteger xy = x.subtract(y).abs();
            int maxLenght = Math.max(x.toString().length(), y.toString().length());
            int count =maxLenght - xy.toString().length();

            String res = "";
            while(count-->0){
                res += "0";
            }
            res+=xy.toString();
            System.out.println(res);
        }

    }
}

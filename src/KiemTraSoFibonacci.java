import java.util.Scanner;

public class KiemTraSoFibonacci {
    public static boolean solve(long n){
        if(n == 0){
            return true;
        }
        long a = 1;
        long b = 1;
        long c = 1;
        while (c < n) {
            c = a + b;
            a = b;
            b = c;
        }
        if (c == n) {
            return true;
        }
        else
            return false;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long t = sc.nextInt();
        for (int i = 0; i < t; i++) {
            long n = sc.nextLong();
            if(solve(n)){
                System.out.println("YES");
            }
            else
                System.out.println("NO");
        }
    }
}

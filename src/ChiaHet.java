import java.math.BigInteger;
import java.util.Scanner;

public class ChiaHet {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String s1 = sc.next();
            String s2 = sc.next();
            BigInteger b1 = new BigInteger(s1);
            BigInteger b2 = new BigInteger(s2);

            if (b1.equals(b1.divide(b2).multiply(b2)) || b2.equals(b2.divide(b1).multiply(b1))) {
                System.out.println("YES");
            } else
                System.out.println("NO");
        }
    }
}

import java.util.Scanner;

public class XauNhiPhan {
    public static long[] Fibo = new long[93];

    public static void initFibo() {
        Fibo[1] = 1;
        Fibo[2] = 1;
        for(int i = 3; i <= 92; i++) {
            Fibo[i] = Fibo[i-2] + Fibo[i-1];
        }
    }


    public static Character solve(int n, long k) {
        if (n == 1) {
            return '0';
        }
        if (n == 2) {
            return '1';
        }
        if (k > Fibo[n - 2]) {
            return solve(n - 1, (long)k - Fibo[n - 2]);
        }
        else
            return solve(n - 2, k);

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        initFibo();
        for (int i = 0; i < t; i++) {
            int n = sc.nextInt();
            long k = sc.nextLong();
            System.out.println(solve(n,k));

        }
    }
}

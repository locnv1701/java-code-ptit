import java.util.*;

public class GiaoCuaHaiDaySo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        Set<Integer> st1 = new HashSet<>();
        for (int i = 0; i < n; i++) {
            st1.add(sc.nextInt());
        }
        Set<Integer> st2 = new HashSet<>();
        for (int i = 0; i < m; i++) {
            int tmp = sc.nextInt();
            if (st1.contains(tmp)) {
                st2.add(tmp);
            }
        }

        List<Integer> list = new ArrayList<>(st2);
        Collections.sort(list);
        for (int a : list) {
            System.out.print(a + " ");
        }
    }
}

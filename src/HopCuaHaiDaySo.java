import java.util.*;

public class HopCuaHaiDaySo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        Set<Integer> st1 = new HashSet<>();
        for (int i = 0; i < n + m; i++) {
            st1.add(sc.nextInt());
        }


        List<Integer> list = new ArrayList<>(st1);
        Collections.sort(list);
        for (int a : list) {
            System.out.print(a + " ");
        }
    }
}

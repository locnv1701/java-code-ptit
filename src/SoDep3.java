import java.util.Scanner;

public class SoDep3 {
    public static boolean prime(int n) {
        if (n < 2) {
            return false;
        }
        for (int i = 2; i < Math.sqrt(n); i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            String str = sc.next();
            boolean check = true;
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
                    check = false;
//                    System.out.println("loi thuan nghich tai " + i);
                    break;
                }
                if (!prime(Character.getNumericValue(str.charAt(i)))) {
                    check = false;
                    break;
                }
            }

            if (check == true) {
                System.out.println("YES");
            } else
                System.out.println("NO");
        }
    }
}

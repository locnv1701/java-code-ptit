import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class DayConCoKPhanTuTangDan {
    public static int n;
    public static int k;
    public static int[] X;
    public static ArrayList<String> arrayList = new ArrayList<>();
    public static ArrayList<Integer> arr = new ArrayList<>();

    public static boolean check() {
        int count = 0;
        for (int i = 0; i < X.length; i++) {
            count += X[i];
        }
        return count == k;
    }

    public static void print() {
        ArrayList<Integer> s = new ArrayList<>();
        if (!check())
            return;
        for (int j = 1; j <= n; j++)
            if (X[j] == 1)
                s.add(arr.get(j - 1));
        arrayList.add(s.toString().replaceAll("\\[", "").replaceAll("]", "").replaceAll(",", "").trim());
    }

    public static void BackTrack(int i) {
        for (int j = 0; j <= 1; j++) {
            X[i] = j;
            if (i == n)
                print();
            else
                BackTrack(i + 1);
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0){
            n = sc.nextInt();
            k = sc.nextInt();
            X = new int[16];
            for (int i = 1; i <= n; i++) {
                arr.add(sc.nextInt());
            }
            arr.sort(null);
            BackTrack(1);
            arrayList.sort(null);
            arrayList.forEach(System.out::println);
        }

    }
}


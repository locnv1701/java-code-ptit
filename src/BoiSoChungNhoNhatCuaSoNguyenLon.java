import java.math.BigInteger;
import java.util.Scanner;

public class BoiSoChungNhoNhatCuaSoNguyenLon {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        String tmp = sc.nextLine();
        while(t-->0){
            BigInteger a = new BigInteger(sc.nextLine());
            BigInteger b = new BigInteger(sc.nextLine());
            BigInteger c = a.multiply(b);
            BigInteger d = a.gcd(b);
            BigInteger res = (c).divide(d);
            System.out.println(res);
        }
    }
}

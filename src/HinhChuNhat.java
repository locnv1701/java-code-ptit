import java.util.Scanner;

public class HinhChuNhat {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int height = sc.nextInt();
        int width = sc.nextInt();
        if (height <= 0 || width <= 0) {
            System.out.println(0);
        } else
            System.out.println((height + width) * 2 + " " + width * height);

    }
}

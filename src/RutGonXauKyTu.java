import java.util.Scanner;
import java.util.Stack;

public class RutGonXauKyTu {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String string = sc.nextLine();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < string.length(); i++) {
            if (stack.empty()) {
                stack.push(string.charAt(i));
            } else if (stack.peek() == string.charAt(i)) {
                stack.pop();
            } else
                stack.push(string.charAt(i));
        }
        if (stack.empty()) {
            System.out.println("Empty String");
        } else {
            Object[] array = stack.toArray();
            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i]);
            }
        }
    }

}

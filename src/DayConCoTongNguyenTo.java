import java.util.*;

public class DayConCoTongNguyenTo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            ArrayList<Integer> arr = new ArrayList<>();
            int[] tmp = new int[16];
            for (int j = 0; j < n; j++) {
                arr.add(sc.nextInt());
            }
            arr.sort(Comparator.reverseOrder());
            quinn(arr, 0, tmp, n);
        }
    }

    public static void quinn(ArrayList<Integer> arr, int i, int[] tmp, int n) {
        for (int j = 0; j < 2; j++) {
            tmp[i] = j;
            if (i == n - 1) {
                if (check(arr, tmp, n)) {
                    for (int k = 0; k < n; k++) {
                        if (tmp[k] == 1)
                            System.out.print(arr.get(k) + " ");
                    }
                    System.out.println();
                }
            } else {
                quinn(arr, i + 1, tmp, n);
            }
        }
    }

    public static boolean Prime(int n) {
        if (n < 2) {
            return false;
        }
        int squareRoot = (int) Math.sqrt(n);
        for (int i = 2; i <= squareRoot; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean check(ArrayList<Integer> arr, int[] tmp, int n) {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            if (tmp[i] == 1)
                sum += arr.get(i);
        }
        return Prime(sum);
    }
}
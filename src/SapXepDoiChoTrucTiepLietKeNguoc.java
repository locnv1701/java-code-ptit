import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SapXepDoiChoTrucTiepLietKeNguoc {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0) {
            int n = sc.nextInt();
            int a[] = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = sc.nextInt();
            }
            sort(a, n);
        }
    }

    public static void sort(int a[], int n) {
        int t;
        ArrayList<String> res = new ArrayList<>();
        for (int i = 0; i <n - 1;  i++) {
            for (int j = i+1; j <n; j++) {
                if (a[i] > a[j]) {
                    t = a[i];
                    a[i] = a[j];
                    a[j] = t;
                }
            }
            String tmp = "Buoc "+ (i+1) + ":";
            for (int j = 0; j < n; j++) {
                tmp = tmp + " " + a[j];
            }
            res.add(tmp);
        }
        Collections.reverse(res);
        res.forEach(System.out::println);
    }
}

import java.util.Scanner;

public class DayConLienTiepTongBangK {
    public static boolean solve(int[] array, int n, long k){
        long sum = 0;
        int start = 0;
        for (int i = 0; i < n; i++) {
            sum += array[i];
            while(sum - array[start] >= k){
                sum -= array[start];
                start++;
            }
            if(sum == k){
                return true;
            }
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0){
            int n = sc.nextInt();
            long k = sc.nextLong();
            int[] array = new int[n];
            int check = 0;
            for (int i = 0; i < n; i++) {
                array[i] = sc.nextInt();
                if(array[i] == 0){
                    check = 1;
                }
            }
            if((k==0 && check == 1) || (k != 0 && solve(array, n, k))){
                System.out.println("YES");
            }
            else
                System.out.println("NO");
        }
    }
}

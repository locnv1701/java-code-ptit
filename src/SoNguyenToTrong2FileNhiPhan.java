import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class SoNguyenToTrong2FileNhiPhan {
    public static boolean Prime(int n) {
        if (n < 2) {
            return false;
        }
        int squareRoot = (int) Math.sqrt(n);
        for (int i = 2; i <= squareRoot; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) throws Exception {
        FileInputStream fileInp = new FileInputStream("NHIPHAN.in");
        ObjectInputStream objIS = new ObjectInputStream(fileInp);
        ArrayList<Integer> a = (ArrayList<Integer>) objIS.readObject();
        objIS.close();

        fileInp = new FileInputStream("DATA2.in");
        objIS = new ObjectInputStream(fileInp);
        ArrayList<Integer> b = (ArrayList<Integer>) objIS.readObject();
        objIS.close();

        int[] fibo1 = new int[10000];
        int[] fibo2 = new int[10000];

        Arrays.fill(fibo1, 0);
        Arrays.fill(fibo2, 0);
        for (Integer integer : a) {
            if (Prime(integer)) {
                fibo1[integer]++;
            }
        }
        for (Integer integer : b) {
            if (Prime(integer)) {
                fibo2[integer]++;
            }
        }
        for (int i = 0; i < 10000; i++) {
            if (fibo1[i] != 0 && fibo2[i] != 0) {
                System.out.println(i + " " + fibo1[i] + " " + fibo2[i]);
            }
        }
    }
}

/*
ArrayList<Integer> arr = new ArrayList<>();
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        try {   // dat try cacth de tranh ngoai le khi tao va viet File
            FileOutputStream f = new FileOutputStream("NHIPHAN.in"); // tao file f tro den student.dat
            ObjectOutputStream oStream = new ObjectOutputStream(f); // dung de ghi theo Object vao file f
            oStream.writeObject(arr); // ghi MyStudent theo kieu Object vao file
            oStream.close();
        } catch (IOException e) {
            System.out.println("Error Write file");
        }

        ArrayList<Integer> brr = new ArrayList<>();
        brr.add(2);
        brr.add(2);
        brr.add(2);
        brr.add(2);
        brr.add(2);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(1);
        brr.add(1);
        brr.add(1);

        brr.add(1);
        try {   // dat try cacth de tranh ngoai le khi tao va viet File
            FileOutputStream f = new FileOutputStream("DATA2.in"); // tao file f tro den student.dat
            ObjectOutputStream oStream = new ObjectOutputStream(f); // dung de ghi theo Object vao file f
            oStream.writeObject(brr); // ghi MyStudent theo kieu Object vao file
            oStream.close();
        } catch (IOException e) {
            System.out.println("Error Write file");
        }
 */

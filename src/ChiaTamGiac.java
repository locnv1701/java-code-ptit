import java.util.Scanner;

public class ChiaTamGiac {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n, h;
            n = sc.nextInt();
            h = sc.nextInt();
            for (int i = 1; i < n; i++) {
                double res = h * Math.sqrt((double) i / n);
                System.out.printf("%.6f ",res);
            }
            System.out.println();

        }
    }
}

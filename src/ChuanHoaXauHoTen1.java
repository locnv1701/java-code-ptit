import java.util.Locale;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ChuanHoaXauHoTen1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        String tmp = sc.nextLine();
        while (t-- > 0) {
            String str = sc.nextLine();
            str = str.toLowerCase();
            str = str.trim();
            str = str.replaceAll("\\s+", " ");
            String[] token = str.split(" ");
            String res = "";
            for (int i = 0; i < token.length; i++) {
                res += String.valueOf(token[i].charAt(0)).toUpperCase() + token[i].substring(1,token[i].length());
                res += " ";
            }
            System.out.println(res);
        }
    }
}

import java.util.Scanner;

public class MaTranNhiPhan {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        int count;
        int quinn = 0;
        for (int i = 0; i < t; i++) {
            count = 0;
            for (int j = 0; j < 3; j++) {
                count+= sc.nextInt();
            }
            if(count >= 2)
                quinn++;
        }
        System.out.println(quinn);
    }
}

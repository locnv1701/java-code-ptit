import java.util.Arrays;
import java.util.Scanner;

public class SoXaCach {
    public static int n;
    public static int X[], unused[];

    public static void print() {
        for (int j = 1; j < n; j++) {
            if (Math.abs(X[j] - X[j + 1]) == 1)
                return;
        }

        for (int j = 1; j <= n; j++)
            System.out.print(X[j]);
        System.out.println();
    }

    public static void BackTrack(int i) { //Liet ke cac hoan vi cua 1..n bat dau tu X[i]
        //Xet cac kha nang cua X[i]
        for (int j = 1; j <= n; j++) {
            if (unused[j] == 1) {                  //Xet kha nang j da duoc su dung chua?
                X[i] = j;      //chua duoc su dung, j duoc chap thuan
                unused[j] = 0; //danh dau kha nang j da duoc su dung
                if (i == n)    //X[i] la thanh phan cuoi cung, in cau hinh
                    print();
                else //X[i] chua phai la thanh phan cuoi, sinh tiep bat dau tu X[i+1]
                    BackTrack(i + 1);
                unused[j] = 1; //tra lai kha nang j chua duoc su dung
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            n = sc.nextInt();
            X = new int[n + 1];
            unused = new int[n + 1];
            Arrays.fill(unused, 1);
            BackTrack(1);

        }
    }
}

import java.util.Arrays;
import java.util.Scanner;

public class BoBaSoPitago {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            long[] arr = new long[n];
            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
            }
            Arrays.sort(arr);
            System.out.println(quinnCheck(arr,n));
        }
    }

    public static String quinnCheck(long arr[], int n) {
        for (int i = 0; i < n; i++) {
            arr[i] = arr[i] * arr[i];
        }
        for (int i = n - 1; i >= 2; i--) {
            int l = 0, r = i - 1;
            while (l < r) {
                if (arr[l] + arr[r] == arr[i]) {
                    return "YES";
                } else {
                    if (arr[l] + arr[r] < arr[i]) l++;
                    else r--;
                }
            }
        }
        return "NO";
    }
}
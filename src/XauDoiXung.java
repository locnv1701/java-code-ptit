import java.util.Scanner;

public class XauDoiXung {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0){
            String str = sc.next();
            if(str.length() % 2 == 0) {
                int check = 2;
                int sum = 0;
                for (int i = 0; i < str.length()/2; i++) {
                    if(str.charAt(i) != str.charAt(str.length()-1 - i)) {
                        check -= 1;
                        if(check == 0){
                            break;
                        }
                    }
                }
                if(check == 1 ){
                    System.out.println("YES");
                }
                else
                    System.out.println("NO");
            }
            else {
                int check = 2;
                int sum = 0;
                for (int i = 0; i < str.length()/2; i++) {
                    if(str.charAt(i) != str.charAt(str.length()-1 - i)) {
                        check -= 1;
                        if(check == 0){
                            break;
                        }
                    }
                }
                if(check > 0 ){
                    System.out.println("YES");
                }
                else
                    System.out.println("NO");
            }

        }
    }
}

import java.util.ArrayList;
import java.util.Scanner;

public class ChuanHoaCau {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> quinn = new ArrayList<>();
        while (sc.hasNext()) {
            String line = sc.nextLine().toLowerCase().replaceAll("\\s+", " ");
            if (line.equals(""))
                break;
            char c = line.charAt(line.length() - 1);
            if (c != '.' && c != '!' && c != '?')
                line += ".";

            String res = "";
            String[] word = line.split(" ");
            res += Character.toUpperCase(word[0].charAt(0)) + word[0].substring(1);
            res += " ";

            for (int i = 1; i < word.length - 1; i++) {
                c = word[i - 1].charAt(word[i - 1].length() - 1);
                if (c == '.' || c == '!' || c == '?') {
                    res+=(Character.toUpperCase(word[0].charAt(0))) + (word[0].substring(1));
                } else
                    res += (word[i]);
                if (!word[i + 1].equals(".") && !word[i + 1].equals("?") && !word[i + 1].equals("!"))
                    res+=(" ");
            }
            res+=(word[word.length - 1]);
            quinn.add(res);
        }
        quinn.forEach(System.out::println);
    }
}

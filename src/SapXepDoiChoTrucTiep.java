import  java.util.Scanner;

public class SapXepDoiChoTrucTiep {
    public static void output(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static void swap(int arr[], int i, int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        int count = 0;
        for (int i = 0; i < n - 1; i++) {
            int flag = 0;
            for (int j = i+1; j < n; j++) {
                if(arr[i] > arr[j]){
                    swap(arr, i, j);
                    flag = 1;
                }
            }
            if(flag == 1) {
                count++;
                System.out.print("Buoc " + count + ": ");
                output(arr,n);
            }
        }
    }
}

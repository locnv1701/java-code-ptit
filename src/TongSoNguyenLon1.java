import java.util.Scanner;
import java.math.BigInteger;
public class TongSoNguyenLon1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0){
            BigInteger x = new BigInteger(sc.next());
            BigInteger y = new BigInteger(sc.next());

            BigInteger xy = x.add(y);
            System.out.println(xy);
        }

    }
}

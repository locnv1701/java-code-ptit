import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class SoNguyenToTrongFileNhiPhan {
    public static boolean Prime(int n) {
        if (n < 2) {
            return false;
        }
        int squareRoot = (int) Math.sqrt(n);
        for (int i = 2; i <= squareRoot; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) throws Exception {
        FileInputStream fileInp = new FileInputStream("SONGUYEN.in");
        ObjectInputStream objIS = new ObjectInputStream(fileInp);
        ArrayList<Integer> a = (ArrayList<Integer>) objIS.readObject();
        a.sort(null);
        int i = 0;
        while (i < a.size()) {
            i++;
            if (Prime(a.get(i-1))) {
                int count = 1;
                while (i < a.size() && a.get(i - 1).equals(a.get(i))) {
                    i++;
                    count++;
                }
                System.out.printf("%d %d\n", a.get(i - 1), count);
            }
        }
        objIS.close();
    }
}
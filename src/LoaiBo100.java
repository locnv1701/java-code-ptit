//import java.util.Scanner;
//import java.util.Stack;
//
//public class LoaiBo100 {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        int t = sc.nextInt();
//        String tmp = sc.nextLine();
//        while(t-->0){
//            String string = sc.nextLine();
//
//            Stack<Character> stack = new Stack<>();
//            stack.push(string.charAt(0));
//            Character preTop = stack.peek();
//            stack.push(string.charAt(1));
//            Character top = stack.peek();
//            int count = 0;
//
//            for (int i = 2; i < string.length(); i++) {
//                if(stack.size() == 1){
//                    preTop = stack.peek();
//                    stack.push(string.charAt(i));
//                    top = stack.peek();
//                }
//                if(stack.empty()) {
//                    stack.push(string.charAt(i));
//                    top = stack.peek();
//                }
//                else if(preTop == '1' && top == '0' && string.charAt(i) == '0') {
//                    count += 1;
//                    stack.pop();
//                    stack.pop();
//                    if(stack.size() == 1){
//                        top = stack.peek();
//                    }
//                    else if(stack.size() >= 2) {
//                        top = stack.peek();
//                        stack.pop();
//                        preTop = stack.peek();
//                        stack.push(top);
//                    }
//                }
//                else {
//                    preTop = top;
//                    stack.push(string.charAt(i));
//                    top = stack.peek();
//                }
//            }
//            System.out.println(count*3);
//        }
//    }
//}


import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class LoaiBo100 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        String tmp = sc.nextLine();
        while (t-- > 0) {
            String string = sc.nextLine();
            int[] count = new int[string.length()];
            int result = 0;
            int count1 = 0;
            Arrays.fill(count, 0);
            for (int i = string.length() - 1; i >= 0; i--) {
                if (string.charAt(i) == '0') {
                    count1 += 1;
                }
                if (string.charAt(i) == '1') {
                    count[i] = count1;
                }
            }
            for (int i = string.length() - 1; i >= 0; i--) {
                if(count[i] - result*2 >= 2){
                    result += 1;
                }
            }
            System.out.println(result*3);

        }
    }
}


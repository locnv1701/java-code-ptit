import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class DanhDauChuCai {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        Set<String> set = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            set.add(s.substring(i, i + 1));
        }
        System.out.println(set.size());
    }
}

import java.util.Scanner;

import static java.lang.Math.abs;

public class CatDoi {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String str = sc.nextLine();
            boolean quinn = true;
            for (int i = 0; i < str.length() - 1; i++) {
                if (abs((int) str.charAt(i) - (int) str.charAt(i + 1)) != 1) {
                    quinn = false;
                    break;
                }
            }
            if (quinn)
                System.out.println("YES");
            else
                System.out.println("NO");
        }
    }
}


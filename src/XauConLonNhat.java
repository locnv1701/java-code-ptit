import java.util.Scanner;

public class XauConLonNhat {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        StringBuilder res = new StringBuilder();
        String tmp = "";
        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.substring(i, i + 1).compareTo(tmp) >= 0) {
                tmp = s.substring(i, i + 1);
                res.append(tmp);
            }
        }

        System.out.println(res.reverse());
    }
}

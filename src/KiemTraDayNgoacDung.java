import java.util.Objects;
import java.util.Scanner;
import java.util.Stack;

public class KiemTraDayNgoacDung {
    public static boolean check(String str) {
        Stack<String> s = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '(' || str.charAt(i) == '{' || str.charAt(i) == '[')
                s.push(String.valueOf(str.charAt(i)));
            else {
                if (!s.empty() && str.charAt(i) == ']' && Objects.equals(s.peek(), "["))
                    s.pop();
                else if (!s.empty() && str.charAt(i) == '}' && Objects.equals(s.peek(), "{"))
                    s.pop();
                else if (!s.empty() && str.charAt(i) == ')' && Objects.equals(s.peek(), "("))
                    s.pop();
                else return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String str = sc.nextLine();
            if (check(str))
                System.out.println("YES");
            else
                System.out.println("NO");
        }
    }
}

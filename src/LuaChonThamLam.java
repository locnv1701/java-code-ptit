import java.util.Scanner;

public class LuaChonThamLam {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int S = sc.nextInt();
        if (S == 0 || S > 9 * N) {
            System.out.print("-1 -1");
        } else System.out.print(min(S, N) + " " + max(S, N));
    }

    public static String max(int s, int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = 0;
        }
        for (int i = 0; i < arr.length; i++) {
            if (s < 1) {
                break;
            }
            arr[i] = Math.min(s, 9);
            s -= 9;
        }
        String res = "";
        for (int i = 0; i < n; i++) {
            res += arr[i];
        }
        return res;
    }

    public static String min(int s, int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = 0;
        }
        int count = 0;
        for (int i = arr.length - 1; i >= 0; i--) {
            if (s > 9) {
                arr[i] = 9;
                s -= 9;
            } else {
                arr[i] = s;
                count = i;
                break;
            }
        }
        arr[count]--;
        arr[0] += 1;
        String res = "";
        for (int i = 0; i < n; i++) {
            res += arr[i];
        }
        return res;
    }
}

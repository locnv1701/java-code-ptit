import java.util.Scanner;

public class PhanTuBenPhaiDauTienLonHon {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
            }
            for (int i = 0; i < n; i++) {
                int quinn = 1;
                for (int j = i + 1; j < n; j++) {
                    if (arr[j] > arr[i]) {
                        System.out.print(arr[j] + " ");
                        quinn = 0;
                        break;
                    }
                }
                if (quinn == 1)
                    System.out.print(-1 + " ");

            }
            System.out.println();
        }
    }
}

import java.util.Scanner;

public class UocSoNguyenToLonNhat {
    public static boolean isPrimeNumber(long n) {
        // so nguyen n < 2 khong phai la so nguyen to
        if (n < 2) {
            return false;
        }
        // check so nguyen to khi n >= 2
        long squareRoot = (long) Math.sqrt(n);
        for (long i = 2; i <= squareRoot; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            long n = sc.nextLong();
            long result = 0;
            int check = 1;
            for (long i = 1; i <= Math.sqrt(n); i++) {
                if (n % i == 0) {
                    if (isPrimeNumber(n / i)) {
                        System.out.println(n / i);
                        check = 0;
                        break;
                    } else if (isPrimeNumber(i)) {
                        result = i;
                    }
                }
            }
            if (check == 1)
                System.out.println(result);
        }
    }
}

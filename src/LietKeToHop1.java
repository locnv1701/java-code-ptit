import java.util.Scanner;

public class LietKeToHop1 {
    public static int n, k = 0, count = 0;
    public static boolean cauhinhcuoi = false;
    public static int[] X = new int[100];

    public static void sinh() {
        int i = k;
        while ((i > 0) && (X[i] == n - k + i))
            i--;

        if (i == 0) {
            cauhinhcuoi = true;
            return;
        } else {
            X[i] = X[i] + 1;
            for (int j = i + 1; j <= k; j++) {
                X[j] = X[i] + (j - i);
            }
        }
        return;
    }

    public static void print() {
        for (int i = 1; i <= k; i++) {
            System.out.print(X[i]);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        cauhinhcuoi = false;
        n = sc.nextInt();
        k = sc.nextInt();
        for (int i = 1; i <= k; i++) {
            X[i] = i;
        }
        while (!cauhinhcuoi) {
            print();
            System.out.print(" ");
            sinh();
            count++;
        }
        System.out.println();
        System.out.println("Tong cong co " + count + " to hop");

    }
}

import java.util.Scanner;

public class PhanTichThuaSoNguyenTo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 1; i <= t; i++) {
            int n = sc.nextInt();
            System.out.print("Test " + i + ": ");
            for (int j = 2; j <= n ; j++) {
                if(n%j==0) {
                    System.out.print(j);
                    int count = 0;
                    while(n%j==0) {
                        count++;
                        n/=j;
                    }
                    System.out.print("(" + count + ") ");
                }
            }
            System.out.println();



        }
    }
}

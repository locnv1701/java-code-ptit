import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DiaChiEMAIL {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        String tmp = sc.nextLine();
        Map<String, Integer> map = new HashMap<String, Integer>();
        while (t-- > 0) {
            String str = sc.nextLine();
            str = str.toLowerCase().trim().replaceAll("\\s+", " ");
            String[] token = str.split(" ");
            String res = token[token.length - 1];
            for (int i = 0; i < token.length - 1; i++) {
                res += token[i].charAt(0);
            }
            if (map.containsKey(res)) {
                map.put(res, map.get(res) + 1);
                res += map.get(res);
            } else {
                map.put(res, 1);
            }

            res += "@ptit.edu.vn";
            System.out.println(res);
        }
    }
}

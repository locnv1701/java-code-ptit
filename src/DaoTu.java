import java.util.Scanner;

public class DaoTu {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while(t-->0){
            String[] word = sc.nextLine().split("\\s+");
            StringBuilder res = new StringBuilder();
            for (String s : word) {
                StringBuilder sb = new StringBuilder(s);
                res.append(sb.reverse()).append(" ");
            }
            System.out.println(res);
        }
    }
}

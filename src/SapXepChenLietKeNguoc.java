import java.util.*;

public class SapXepChenLietKeNguoc {
    public static void main(String[] args) {
        int a[] = new int[100000];
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }

        String[] res = new String[100];
        int temp, j;
        res[0] = "Buoc 0: " + a[0];
        // System.out.println("Buoc 0: " + a[0]);

        for (int i = 1; i < n; i++) {
            // System.out.print("Buoc " + i + ": ");
            res[i] = "Buoc " + i + ": ";
            temp = a[i];
            j = i;
            while (j > 0 && a[j - 1] > temp) {
                a[j] = a[j - 1];
                j--;
            }
            a[j] = temp;
            for (int k = 0; k <= i; k++) {
                // System.out.print(a[k] + " ");
                res[i] += a[k] + " ";
            }
        }
        for (int i = n - 1; i >= 0; i--) {
            System.out.println(res[i]);
        }
        in.close();
    }
}
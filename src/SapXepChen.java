import java.util.Collections;
import java.util.Scanner;
import java.util.Vector;

public class SapXepChen {
    public static void output(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        Vector<Integer> vec = new Vector<Integer>();
        for (int i = 0; i < n; i++) {
            vec.add(arr[i]);
            Collections.sort(vec);
            System.out.print("Buoc " + i + ": ");
            for (int num : vec) {
                System.out.print(num + " ");
            }
            System.out.println();
        }
    }
}


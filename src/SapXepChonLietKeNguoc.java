import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SapXepChonLietKeNguoc {
    public static void swap(int arr[], int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        ArrayList<String> res = new ArrayList<>();
        for (int i = 0; i < n - 1; i++) {
            int index = i + 1;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[index]) {
                    index = j;
                }
            }
            if (arr[i] > arr[index]) {
                swap(arr, i, index);

            }int step = i + 1;
            String tmp = "Buoc " + step + ":";
            for (int k = 0; k < n; k++) {
                tmp = tmp + " " + arr[k];
            }
            res.add(tmp);
        }
        Collections.reverse(res);
        res.forEach(System.out::println);

    }
}

import java.util.Scanner;

public class TinhLuyThua {
    public static final double modulo = 1e9 + 7;

    public static long luyThua(int a, long b) {
        if (b == 0) {
            return 1;
        }
        if (b == 1) {
            return a;
        }
        long tmp = luyThua(a, b / 2);
        tmp = (long) ((tmp * tmp) % modulo);
        if (b % 2 == 1) {
            return (long) (tmp * a % modulo);
        } else {
            return (long) (tmp % modulo);
        }
    }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            int a = sc.nextInt();
            long b = sc.nextLong();
            if (a == b && b == 0)
                break;
            else {
                System.out.println(luyThua(a, b));
            }
        }
    }
}

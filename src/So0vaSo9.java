import java.util.*;

public class So0vaSo9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            Queue<Integer> q = new ArrayDeque<Integer>();
            q.add(9);
            int n = Integer.parseInt(sc.nextLine());
            while (true) {
                if (q.peek() % n == 0) {
                    System.out.println(q.peek());
                    break;
                }
                int top = q.peek();
                q.remove();
                q.add(top * 10);
                q.add(top * 10 + 9);
            }
        }
    }
}

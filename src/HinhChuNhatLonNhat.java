import java.util.Scanner;
import java.util.Stack;

import static java.lang.Math.max;

public class HinhChuNhatLonNhat {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
            }
            Stack<Integer> st = new Stack<>();
            long res = 0;
            int check;
            for (int i = 0; i < n; i++) {
                if (st.empty()) {
                    st.push(i);
                } else if (arr[st.peek()] <= arr[i]) {
                    st.push(i);
                } else {
                    int x = st.peek();
                    st.pop();
                    if (st.empty()) {
                        check = arr[x] * i;
                    } else {
                        check = arr[x] * (i - st.peek() - 1);
                    }
                    res = max(res, check);
                    i--;
                }
            }
            while (!st.empty()) {
                int x = st.peek();
                st.pop();
                if (st.empty()) {
                    check = arr[x] * n;
                } else {
                    check = arr[x] * (n - st.peek() - 1);
                }
                res = max(check, res);
            }
            System.out.println(res);
        }
    }
}

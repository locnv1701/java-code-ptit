import java.util.Scanner;
public class TongUocSo {
    public static final int MAX = 2000001;
    public static int[] spf = new int[MAX];

    public static void sieve() {
        spf[1] = 1;
        for (int i = 1; i < MAX; i++) {
            spf[i] = i;
        }
        for (int i = 2; i < MAX; i += 2) {
            spf[i] = 2;
        }
        for (int i = 3; i * i < MAX; i++) {
            if (spf[i] == i) {
                for (int j = i * i; j < MAX; j += i) {
                    if (spf[j] == j) {
                        spf[j] = i;
                    }
                }
            }
        }
    }

    public static long res(int x) {
        int sum = 0;
        while (x != 1) {
            sum += spf[x];
            x/=spf[x];
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        sieve();
        int t = sc.nextInt();
        long sum = 0;
        for (int i = 0; i < t; i++) {
            int n = sc.nextInt();
            sum += res(n);
        }
        System.out.println(sum);

    }
}
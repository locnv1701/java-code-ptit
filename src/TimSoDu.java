import javax.security.auth.callback.CallbackHandler;
import java.util.Scanner;

public class TimSoDu {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        while (t-- > 0) {
            String tmp = sc.nextLine();
            String n = sc.next();
            if (n.length() >= 2) {
                int a = Character.getNumericValue(n.charAt(n.length() - 2)) * 10 + Character.getNumericValue(n.charAt(n.length() - 1));
                if (a % 4 == 0) {
                    System.out.println("4");
                } else
                    System.out.println("0");
            } else {
                int c = Character.getNumericValue(n.charAt(0));
                if (c % 4 == 0) {
                    System.out.println("4");
                } else
                    System.out.println("0");
            }
        }


    }
}

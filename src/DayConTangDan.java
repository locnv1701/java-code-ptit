import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class DayConTangDan {
    public static int n;
    public static int[] X;
    public static ArrayList<String> arrayList = new ArrayList<>();
    public static ArrayList<Integer> arr = new ArrayList<>();

    public static boolean check(ArrayList<Integer> s) {
        if (s.size() < 2)
            return false;
        for (int i = 0; i < s.size() - 1; i++) {
            if (s.get(i) > s.get(i + 1))
                return false;
        }
        return true;
    }

    public static void print() {
        ArrayList<Integer> s = new ArrayList<>();
        for (int j = 1; j <= n; j++)
            if (X[j] == 1)
                s.add(arr.get(j - 1));
        if (check(s))
            arrayList.add(s.toString().replaceAll("\\[", "").replaceAll("]", "").replaceAll(",", "").trim());
    }

    public static void BackTrack(int i) {
        for (int j = 0; j <= 1; j++) {
            X[i] = j;
            if (i == n)
                print();
            else
                BackTrack(i + 1);
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("DAYSO.in"));
        n = sc.nextInt();
        X = new int[102];
        for (int i = 1; i <= n; i++) {
            arr.add(sc.nextInt());
        }
        BackTrack(1);
        arrayList.sort(null);
        arrayList.forEach(System.out::println);
    }
}

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class SoNguyenToLonNhatTrongFile {
    public static boolean Prime(int n) {
        if (n < 2) {
            return false;
        }
        int squareRoot = (int) Math.sqrt(n);
        for (int i = 2; i <= squareRoot; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) throws Exception {
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(939);
        arr.add(939);
        arr.add(939);
        arr.add(939);
        arr.add(939);
        arr.add(939);
        arr.add(939);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        try {   // dat try cacth de tranh ngoai le khi tao va viet File
            FileOutputStream f = new FileOutputStream("NHIPHAN.in"); // tao file f tro den student.dat
            ObjectOutputStream oStream = new ObjectOutputStream(f); // dung de ghi theo Object vao file f
            oStream.writeObject(arr); // ghi MyStudent theo kieu Object vao file
            oStream.close();
        } catch (IOException e) {
            System.out.println("Error Write file");
        }
        FileInputStream fileInp = new FileInputStream("DATA.in");
        ObjectInputStream objIS = new ObjectInputStream(fileInp);
        ArrayList<Integer> a = (ArrayList<Integer>) objIS.readObject();
        objIS.close();

        int[] fibo1 = new int[1000007];

        Arrays.fill(fibo1, 0);

        for (Integer integer : a) {
            if (Prime(integer)) {
                fibo1[integer]++;
            }
        }
        int count = 10;
        for (int i = 1000000; i > 0; i--) {
            if (fibo1[i] != 0 && count > 0) {
                count--;
                System.out.println(i + " " + fibo1[i]);
            }
        }
    }
}

/*
ArrayList<Integer> arr = new ArrayList<>();
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(2);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(7);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        arr.add(1);
        try {   // dat try cacth de tranh ngoai le khi tao va viet File
            FileOutputStream f = new FileOutputStream("NHIPHAN.in"); // tao file f tro den student.dat
            ObjectOutputStream oStream = new ObjectOutputStream(f); // dung de ghi theo Object vao file f
            oStream.writeObject(arr); // ghi MyStudent theo kieu Object vao file
            oStream.close();
        } catch (IOException e) {
            System.out.println("Error Write file");
        }

        ArrayList<Integer> brr = new ArrayList<>();
        brr.add(2);
        brr.add(2);
        brr.add(2);
        brr.add(2);
        brr.add(2);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(7);
        brr.add(1);
        brr.add(1);
        brr.add(1);

        brr.add(1);
        try {   // dat try cacth de tranh ngoai le khi tao va viet File
            FileOutputStream f = new FileOutputStream("DATA2.in"); // tao file f tro den student.dat
            ObjectOutputStream oStream = new ObjectOutputStream(f); // dung de ghi theo Object vao file f
            oStream.writeObject(brr); // ghi MyStudent theo kieu Object vao file
            oStream.close();
        } catch (IOException e) {
            System.out.println("Error Write file");
        }
 */

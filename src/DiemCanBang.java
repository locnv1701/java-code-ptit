import java.util.Scanner;

public class DiemCanBang {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            int[] arr = new int[n];
            int sum = 0;
            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
                sum += arr[i];
            }
            int mid = -1;
            int sumOfLeft = 0;
            int sumOfRight = sum - arr[0];
            for (int i = 1; i < n - 1; i++) {
                sumOfLeft += arr[i-1];
                sumOfRight -= arr[i];
                if (sumOfLeft == sumOfRight) {
                    mid = i + 1;
                    break;
                }

            }
            System.out.println(mid);
        }
    }
}



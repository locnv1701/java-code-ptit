import java.util.*;
public class TongGiaiThua {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        long sum = 0;
        long ans = 1;
        for(int i = 1; i <= n; i++){
            ans = ans*i;
            sum += ans;
        }
        System.out.println(sum);
    }
}